
from django.http import HttpResponse
from django.utils import simplejson as json

def json_dumps(s):
    return json.dumps(s, ensure_ascii = False, indent = 1)

def json_response(json_msg):
    return HttpResponse(json_dumps(json_msg), mimetype = "application/json")
