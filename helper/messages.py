
success = {'success': True}
fail = {'error': True}

login_success = {'success': True, 'message': 'login succeed'}
login_fail = {'error': True, 'message': 'login failed'}

logout_success = {'success': True, 'message': 'logout succeed'}
logout_fail = {'error': True, 'message': 'logout failed'}

register_success = {'success': True, 'message': 'register succeed'}
register_fail = {'error': True, 'message': 'register failed'}
