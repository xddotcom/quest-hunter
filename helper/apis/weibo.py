#!/usr/local/bin/python2.7
# -*- coding: utf-8 -*-

import logging
import urllib2

# Find a JSON parser
try:
    import simplejson as json
except ImportError:
    try:
        from django.utils import simplejson as json
    except ImportError:
        import json

from django.utils.http import urlencode


def force_utf8(string):
    """ Detect if a string is unicode and encode as utf-8 if necessary """
    if isinstance(string, unicode):
        string = string.encode('utf-8')
        logging.debug('Convert to utf-8:' + string)
    return string


def http_request(url, args = None, post_args = None):
    args = args or {}
    post_data = None if post_args is None else urlencode(post_args)

    try:
        response = urllib2.urlopen(url + "?" + urlencode(args), post_data)
    except urllib2.HTTPError, e:
        error = json.loads(e.read())
        raise APIError(error)

    try:
        file_info = response.info()
        if file_info.maintype == 'text' or file_info.type == 'application/json':
            result = json.loads(response.read())
        elif file_info.maintype == 'image':
            mimetype = file_info['content-type']
            result = {'data': response.read(), 'mime-type': mimetype, 'url': file.url}
        else:
            raise APIError('Maintype was not text or image')
    finally:
        response.close()

    if result and isinstance(result, dict) and 'error' in result:
        raise APIError(result)
    return result


def auth_url(client_id, redirect_uri, scope = None):
    url = 'https://api.weibo.com/oauth2/authorize?'
    kvps = {'client_id': client_id, 'redirect_uri': redirect_uri, 'display': 'display'}
    if scope:
        kvps['scope'] = ','.join(scope)
    return url + urlencode(kvps)


def get_access_token_from_code(code, client_id, client_secret, redirect_uri):
    """ {'access_token': '***', 'expires_in': ***, 'uid': '***'}  """
    post_args = {'grant_type': 'authorization_code', 'code': code, 'redirect_uri': redirect_uri,
                 'client_id': client_id, 'client_secret': client_secret}
    token_url = 'https://api.weibo.com/oauth2/access_token'
    response = http_request(url = token_url, post_args = post_args)
    try:
        return response['access_token'], response['expires_in'], response['uid']
    except:
        raise APIError('Get access token failed', response)


class ClientAPI(object):

    def __init__(self, secret_key, access_token, uid):
        self.api_url = 'https://api.weibo.com/2/'
        self.secret_key = secret_key
        self.access_token = access_token
        self.uid = uid

    def api_request(self, path, args = None):
        args = args or {}
        args['access_token'] = self.access_token
        url = self.api_url + path
        response = http_request(url = url, args = args)
        return response


class APIError(Exception):
    def __init__(self, message, result = None):
        self.message = message
        self.result = result or '[]'
        Exception.__init__(self, self.message)
    def __str__(self):
        return self.message + ' : ' + json.dumps(self.result, ensure_ascii = False)
