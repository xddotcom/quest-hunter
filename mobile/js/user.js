
function User() {
	this.getLoginLinks = function() {
		$.ajax({
			url: "/user/",
			async: false,
			success: function(json) {
				debug(json);
				User.prototype.weiboLoginLink = json.renren_login_link;
				User.prototype.renrenLoginLink = json.renren_login_link;
			},
		});
	}
	this.getProfile = function() {
		$.ajax({
			url: "/user/profile",
			success: function(json) {
				debug(json);
				this.id = json.id;
				this.name = json.name;
				this.rruid = json.rruid;
				this.wbuid = json.wbuid;
				$("#output").html(JSON.stringify(json));
			},
		});
	}
	this.login = function(data) {
		debug("login");
		$.ajax({
			type: "POST",
			url: "/user/login/",
			data: data,
			success: function(json) {
				debug(json);
				if (json.success) {}
			},
		});
	}
	this.logout = function() {
		$.ajax({
			type: "POST",
			url: "/user/logout/",
			success: function(json) {
				debug(json);
				if (json.success) {}
			},
		});
	}
}

function output_json(json) {
	if (json.error) {
		$("#output").html(JSON.stringify(json));
	} else {
		$("#output").html(JSON.stringify(json));
	}
}
