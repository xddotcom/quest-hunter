var doInitOnce = true;

function initialize() {
	if (doInitOnce) {
		doInitOnce = false;
	} else {
		return;
	}

	initUser();
}

function initUser() {
	var user = new User();
	user.getLoginLinks();
	
	$("#renren-login-button").attr("href", user.renrenLoginLink);
	$("#weibo-login-button").attr("href", user.weiboLoginLink);
	
	$("#login-button").on("click", function(event){
		user.login($("#login-form").serialize());
		//user.getProfile();
	});
	
	$("#logout-button").on("click", function(event){
		user.logout();
		$("#popup-logout").popup("open", {transition: "slidedown", positionTo: "#logout-button"});
	});
	
	$("#profile-button").on("click", function(event){
		user.getProfile();
		//$("#output").html();
	});
}

function initModel() {
	debug("Main::initModel");
}

$(document).ready(function() {
	debug("Document is ready");
	initialize();
});
