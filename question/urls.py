from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('question.views',
    url(r'^$', 'index', name = 'index'),
    
    url(r'^show/(?P<qid>\d+)/$', 'show', name = 'show-question-content'),
    url(r'^ask/$', 'ask', name = 'ask-question'),
    url(r'^answer/$', 'answer', name = 'answer-question'),
    
    url(r'^list/$', 'question_list', name = 'list-user-questions'),
)
