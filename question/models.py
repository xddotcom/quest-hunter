from django.db import models
from user.models import User


class QuestionManager(models.Manager):
    pass


class Payment(models.Model):
    amount = models.FloatField()
    creditor = models.ForeignKey(User, related_name = 'creditor')
    debtor = models.ForeignKey(User, related_name = 'debtor')


class Question(models.Model):
    asker = models.ForeignKey(User, related_name = 'asker')
    answer = models.ForeignKey(User, related_name = 'answer')
    payment = models.ForeignKey(Payment)
    content = models.CharField(max_length=1000)
    bid = models.FloatField()

