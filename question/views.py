from django.shortcuts import render_to_response#, redirect

from question.models import Question

from helper.functions import json_response
from helper.decorators import ajax_login_required


import logging
logger = logging.getLogger('questhunter')


def index(request):
    return render_to_response('question/index.html')


@ajax_login_required
def show(request, qid):
    user = request.user
    question = Question.objects.get(pk = qid)
    context = {'question': ''}
    return json_response(context)


@ajax_login_required
def ask(request):
    return render_to_response('question/ask.html')


@ajax_login_required
def answer(request):
    return render_to_response('question/answer.html')


@ajax_login_required
def question_list(request):
    return render_to_response('question/list.html')
