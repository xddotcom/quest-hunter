
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

from user.models import RenrenUser, WeiboUser
from user.oauth import begin_renren, complete_renren, begin_weibo, complete_weibo

from helper.functions import json_response
from helper.decorators import ajax_login_required
from helper import messages

import logging
logger = logging.getLogger('questhunter')


def index(request):
    renren_auth_uri = begin_renren()
    weibo_auth_uri = begin_weibo()
    context = {'renren_login_link': renren_auth_uri, 'weibo_login_link': weibo_auth_uri}
    return json_response(context)


@ajax_login_required
def profile(request):
    user = request.user;
    profile = {'id': user.id, 'name': user.userprofile.name}
    rrusers = user.renrenuser_set.all()
    if rrusers: profile['rruid'] = rrusers[0].uid
    wbusers = user.weibouser_set.all()
    if wbusers: profile['wbuid'] = wbusers[0].uid
    context = {'profile': profile}
    return json_response(context)


def user_register(request):
    try:
        user = User.objects.create_user(username = request.POST['username'],
                                        email = request.POST['email'],
                                        password = request.POST['password'])
        logger.info('User %d created' % user.id)
        return json_response(messages.register_success)
    except Exception as e:
        logger.exception(str(e))
        return json_response(messages.register_fail)


def user_login(request):
    try:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username = username, password = password)
        login(request, user)
        logger.info('User %d logged in' % user.id)
        return json_response(messages.login_success)
    except Exception as e:
        logger.exception(str(e))
        return json_response(messages.login_fail)


def user_logout(request):
    try:
        logout(request)
        logger.info('User logged out')
        return json_response(messages.logout_success)
    except Exception as e:
        logger.exception(str(e))
        return json_response(messages.logout_fail)


def renren_login(request):
    """ Steps: Here comes a renren user A
        1. Try to login A
        2. If user is authenticated
            - If it's not A, merge them (it will put A into database)
            - If it's A, still merge them, this time only update the access_token of A
        3. If user is not authenticated
            - There is neither a local user logged in nor an existing renren user as A
            - Create both of them 
    """
    try:
        uid, access_token, expires_in, refresh_token = complete_renren(request)

        user = authenticate(provider = RenrenUser, identity = uid)
        if user is not None: login(request, user)

        if request.user.is_authenticated():
            RenrenUser.objects.merge_user(uid, access_token, expires_in, refresh_token, user = request.user)
            logger.info('User %d exists, merged with renren user %s' % (request.user.id, uid))
        else:
            RenrenUser.objects.merge_user(uid, access_token, expires_in, refresh_token)
            user = authenticate(provider = RenrenUser, identity = uid)
            login(request, user)
            logger.info('User %d created, merged with renren user %s' % (user.id, uid))

        return json_response(messages.login_success)
    except Exception as e:
        logger.exception(str(e))
        return json_response(messages.login_fail)


def weibo_login(request):
    try:
        uid, access_token, expires_in = complete_weibo(request)

        user = authenticate(provider = WeiboUser, identity = uid)
        if user is not None: login(request, user)

        if request.user.is_authenticated():
            WeiboUser.objects.merge_user(uid, access_token, expires_in, user = request.user)
            logger.info('User %d exists, merged with weibo user %s' % (request.user.id, uid))
        else:
            WeiboUser.objects.merge_user(uid, access_token, expires_in)
            user = authenticate(provider = WeiboUser, identity = uid)
            login(request, user)
            logger.info('User %d created, merged with weibo user %s' % (user.id, uid))

        return json_response(messages.login_success)
    except Exception as e:
        logger.exception(str(e))
        return json_response(messages.login_fail)

