from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('user.views',
    
    url(r'^$', 'index', name = 'login'),
    
    url(r'^profile/$', 'profile', name = 'profile'),
    
    url(r'^register/$', 'user_register', name = 'register'),
    url(r'^login/$', 'user_login', name = 'login'),
    url(r'^logout/$', 'user_logout', name = 'logout'),
    
    url(r'^renren/$', 'renren_login', name = 'renren auth'),
    url(r'^weibo/$', 'weibo_login', name = 'weibo auth'),

)
