from helper.apis import renren, weibo
from django.conf import settings 

RENREN_AUTH = settings.RENREN_AUTH
WEIBO_AUTH = settings.WEIBO_AUTH

def begin_renren():
    scope = RENREN_AUTH['scope']
    api_key = RENREN_AUTH['api_key']
    redirect_uri = RENREN_AUTH['redirect_uri']
    return renren.auth_url(api_key, redirect_uri, scope)

def begin_weibo():
    scope = WEIBO_AUTH['scope']
    api_key = WEIBO_AUTH['api_key']
    redirect_uri = WEIBO_AUTH['redirect_uri']
    return weibo.auth_url(api_key, redirect_uri, scope)


def validate():
    pass


def complete_renren(request):
    access_token, expires_in, refresh_token = \
        renren.get_access_token_from_code(code = request.GET['code'],
                                          client_id = RENREN_AUTH['api_key'],
                                          client_secret = RENREN_AUTH['secret_key'],
                                          redirect_uri = RENREN_AUTH['redirect_uri'])
    client = renren.ClientAPI(secret_key = RENREN_AUTH['secret_key'], access_token = access_token)
    uid = client.get_user_id()
    return uid, access_token, expires_in, refresh_token


def complete_weibo(request):
    access_token, expires_in, uid = \
        weibo.get_access_token_from_code(code = request.GET['code'],
                                         client_id = WEIBO_AUTH['api_key'],
                                         client_secret = WEIBO_AUTH['secret_key'],
                                         redirect_uri = WEIBO_AUTH['redirect_uri'])
    return uid, access_token, expires_in

