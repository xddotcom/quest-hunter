from django.contrib.auth.models import User

class PublicBackend(object):
    """ Add this Authentication Backend to 
        AUTHENTICATION_BACKENDS tuple in your settings.py
    """

    def get_user(self, user_id):
        try:
            return User.objects.get(pk = user_id)
        except User.DoesNotExist:
            return None

    def authenticate(self, provider = None, identity = None):
        """ Authenticate user by public identity.
        """
        if identity:
            try:
                return provider.objects.get(pk = identity).user
            except provider.DoesNotExist:
                return None
        else:
            return None
