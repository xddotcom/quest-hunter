from user.models import UserProfile, RenrenUser, WeiboUser
from django.contrib import admin

admin.site.register(UserProfile)
admin.site.register(RenrenUser)
admin.site.register(WeiboUser)
