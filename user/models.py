from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    name = models.CharField(max_length = 50, blank = True)
    alipay = models.EmailField(blank = True)


class Account(models.Model):
    user = models.OneToOneField(User)
    balance = models.FloatField()


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user = instance)
        Account.objects.create(user = instance, balance = 10)

post_save.connect(create_user_profile, sender = User)


class RenrenManager(models.Manager):
    def merge_user(self, uid, access_token, expires_in, refresh_token, user = None):
        if user is not None:
            renren_user = RenrenUser(user = user, uid = uid, access_token = access_token,
                                     expires_in = expires_in, refresh_token = refresh_token)
            renren_user.save()
        else:
            username = 'renren_%d' % uid
            user = User.objects.create_user(username = username)
            renren_user = RenrenUser(user = user, uid = uid, access_token = access_token,
                                     expires_in = expires_in, refresh_token = refresh_token)
            renren_user.save()


class RenrenUser(models.Model):
    uid = models.BigIntegerField(primary_key = True)
    user = models.ForeignKey(User)
    expires_in = models.IntegerField()
    access_token = models.CharField(max_length = 1000)
    refresh_token = models.CharField(max_length = 1000)
    objects = RenrenManager()


class WeiboManager(models.Manager):
    def merge_user(self, uid, access_token, expires_in, user = None):
        if user is not None:
            weibo_user = WeiboUser(user = user, uid = uid, access_token = access_token,
                                   expires_in = expires_in)
            weibo_user.save()
        else:
            username = 'weibo_%d' % uid
            user = User.objects.create_user(username = username)
            weibo_user = WeiboUser(user = user, uid = uid, access_token = access_token,
                                   expires_in = expires_in)
            weibo_user.save()


class WeiboUser(models.Model):
    uid = models.BigIntegerField(primary_key = True)
    user = models.ForeignKey(User)
    expires_in = models.IntegerField()
    access_token = models.CharField(max_length = 1000)
    objects = WeiboManager()

