import os

# The absloute path of this file settings.py
SETTING_DIR = os.path.realpath(os.path.dirname(__file__))
SITE_ROOT = os.path.abspath(os.path.join(SETTING_DIR, '..'))

SITE_URL = "http://quest.ukisama.com/"

DEBUG = False

TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Xindong', 'dxd.spirits@gmail.com'),
)

MANAGERS = ADMINS

ROOT_URLCONF = 'questhunter.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'questhunter.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'questhunter',                # Or path to database file if using sqlite3.
        'USER': 'questhunter',                # Not used with sqlite3.
        'PASSWORD': 'zaq1XSW@cde3',           # Not used with sqlite3.
        'HOST': 'questhunter.db.10206198.hostedresource.com',   # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                           # Set to empty string for default. Not used with sqlite3.
    }
}

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'n(x9)#9ro1lve3-aw$wl2kb2#kzkoec^hmmm8k4q7opu%-60#^'

AUTH_PROFILE_MODULE = 'user.UserProfile'

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'user.PublicBackend',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    #'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'django.middleware.transaction.TransactionMiddleware',
)

MEDIA_ROOT = os.path.join(SITE_ROOT, 'media'),

MEDIA_URL = '/media/'

# This is not for permanent use! It's just the for ease of deployment
# e.g. STATIC_ROOT = '~/html/stc'
# Collectstatic will move all static files here '~/html/stc'
# Then set STATIC_URL = '/stc/' and make this folder accessable in .htaccess
STATIC_ROOT = os.path.join(SITE_ROOT, 'static')

STATIC_URL = '/static/'

# Important staticfiles app is only enabled with runserver and DEBUG = True
# So on a shared server (GoDaddy), use STATIC_ROOT instead
STATICFILES_DIRS = (
    os.path.join(SITE_ROOT, 'staticfiles'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    # 'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

TEMPLATE_DIRS = (
    os.path.join(SITE_ROOT, 'templates'),
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    # 'django.template.loaders.app_directories.Loader',
    # 'django.template.loaders.eggs.Loader',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    
    'django.contrib.admin',
    'django.contrib.admindocs',
    
    'user',
    'question',
    'hunt',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# http://docs.djangoproject.com/en/dev/topics/logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s %(module)s] %(levelname)s %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'questhunter': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
    }
}


# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
TIME_ZONE = 'America/Chicago'

# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True


#############################################################################
#    try:
#        from settings_local import * #@UnusedWildImport
#    except:
#        pass
#    It won't work, however, if you wish to extend a settings variable 
#    (for example, adding an app to INSTALLED_APPS). 
#    For this, I have found that the following ugly hack seems to do the job.
#############################################################################


try:
    LOCAL_SETTINGS #@UndefinedVariable 
except NameError:
    try:
        from settings_local import * #@UnusedWildImport
    except ImportError:
        pass
# Solution from Rob Golding


RENREN_AUTH = {
    'api_key': '3cc7591abdf2424aa984f2d6dedfd4e0',
    'secret_key': '5c941d09b8984e5996ef39d7be57d3b1',
    'redirect_uri': SITE_URL + 'user/renren/',
    'scope': ['read_user_blog', 'read_user_status', 'read_user_message'],
}

WEIBO_AUTH = {
    'api_key': '1465606341',
    'secret_key': '35651137c2e86ac0e5bf12a36c49e917',
    'redirect_uri': SITE_URL + 'user/weibo/',
    'scope': ['friendships_groups_read', 'friendships_groups_write', 'statuses_to_me_read'],
}
