from django.conf.urls import patterns, include, url
from django.views.generic.simple import direct_to_template

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    
    url(r'^$', direct_to_template, {'template': 'index.html'}),
    url(r'^test$', direct_to_template, {'template': 'test.html'}),
    
    url(r'^user/', include('user.urls'), name='user'),
    url(r'^question/', include('question.urls'), name='question'),
    
    url(r'^hunt/', include('hunt.urls'), name='hunt'),
)

try:
    from django.conf import settings
    from django.conf.urls.static import static
    import os
    if settings.LOCAL_SETTINGS and settings.DEBUG:
        urlpatterns += static('/mobile/', document_root = os.path.join(settings.SITE_ROOT, 'mobile'),)
except:
    pass
