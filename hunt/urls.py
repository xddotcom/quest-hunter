from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('hunt.views',
    
    url(r'^$', 'index'),
    
    url(r'^renren/$', 'renren'),
    url(r'^weibo/$', 'weibo'),
    
)
