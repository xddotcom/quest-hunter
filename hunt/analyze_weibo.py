
from collections import Counter
import thread

from helper.apis import weibo
from django.conf import settings


import logging
logger = logging.getLogger('questhunter')


def analyze_friendships(uid, client):
#    response = client.api_request(path = 'friendships/friends/bilateral.json',
#                                  args = {"uid": uid, 'count': 10})
#    friend_list = [item['name'] for item in response['users']]

    n = {'n': 4}
    friends = []
    
    def T1(friends, n):
        for page in range(1, 20):
            response = client.api_request('comments/to_me.json', {'count': 200, 'page': page})
            if not response['comments']: break
            logger.info('200 comments to me')
            friends += [comment['user']['name'] for comment in response['comments']]
        n['n']-=1
    def T2(friends, n):
        for page in range(1, 20):
            response = client.api_request('comments/by_me.json', {'count': 200, 'page': page})
            if not response['comments']: break
            logger.info('200 comments by me')
            friends += [comment['status']['user']['name'] for comment in response['comments']]
        n['n']-=1
    def T3(friends, n):
        for page in range(1, 20):
            response = client.api_request('comments/mentions.json', {'count': 200, 'page': page})
            if not response['comments']: break
            logger.info('200 comments mentioned me')
            friends += [comment['user']['name'] for comment in response['comments']]
        n['n']-=1
    def T4(friends, n):
        for page in range(1, 20):
            response = client.api_request('statuses/mentions.json', {'count': 200, 'page': page})
            if not response['statuses']: break
            logger.info('200 statuses mentioned me')
            friends += [comment['user']['name'] for comment in response['statuses']]
        n['n']-=1
    
    thread.start_new_thread(T1, (friends, n))
    thread.start_new_thread(T2, (friends, n))
    thread.start_new_thread(T3, (friends, n))
    thread.start_new_thread(T4, (friends, n))
    
    while (n['n'] > 0): pass
    
    logger.info('DONE')
    
    friendship = Counter(friends)
    friendship = sorted(friendship.items(), key = lambda x: x[1], reverse = True)
    return friendship


def analyze(weibouser):
    uid, access_token = weibouser.uid, weibouser.access_token
    client = weibo.ClientAPI(secret_key = settings.WEIBO_AUTH['secret_key'],
                             access_token = access_token, uid = uid)

    friendship = analyze_friendships(uid, client)
    return friendship


if __name__ == '__main__':
    analyze()
