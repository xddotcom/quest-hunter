from django.db import models
#from django.contrib.auth.models import User

#class Attributes(models.Model):
#    user = models.ForeignKey(User)
#    key = models.CharField(max_length = 50)
#    value = models.CharField(max_length = 100)
#
#
#class Locations(models.Model):
#    user = models.ForeignKey(User)
#    name = models.CharField(max_length = 50)
#    latitude = models.FloatField()
#    longitude = models.FloatField()


class WeiboStatus(models.Model):
    uid = models.BigIntegerField()
    text = models.CharField(max_length = 1000)


class WeiboFriendships(models.Model):
    user1 = models.BigIntegerField()
    user2 = models.BigIntegerField()
    score = models.IntegerField()
