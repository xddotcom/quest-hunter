
from helper.functions import json_response
from helper.decorators import ajax_login_required
from helper import messages

from hunt import analyze_weibo

import logging
logger = logging.getLogger('questhunter')


@ajax_login_required
def index(request):
    return json_response(messages.success)


@ajax_login_required
def renren(request, uid):
    pass


@ajax_login_required
def weibo(request):
    user = request.user
    weibousers = user.weibouser_set.all()
    friendship = analyze_weibo.analyze(weibousers[0])
    return json_response(friendship)

