
from django.conf import settings 

RENREN_AUTH = settings.RENREN_AUTH

def get_user_info(self):
        response = self.api_request(method = 'users.getInfo')
        try:
            return response[0]['uid']
        except:
            pass
    
def get_friend_list(self):
    pass

def get_status_list(self):
    response = self.api_request(method = 'status.gets', post_args = {'page': 1, 'count': 1000})
    status_list = [item for item in response if 'place' in item]
    return status_list
