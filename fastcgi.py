#!/usr/local/bin/python2.7
# -*- coding: utf-8 -*-

'''
This is a comment, 
If you see this, CGI is not working
import os
print "Content-type: text/html\n\n"
print "Test"
print os.environ
'''
import sys, os

sys.path += ['/home/content/98/10206198/py27venv/lib/python2.7/site-packages']
sys.path += ['/home/content/98/10206198/html/quest/']

os.environ['DJANGO_SETTINGS_MODULE'] = 'questhunter.settings'

from flup.server.fcgi import WSGIServer
from django.core.handlers.wsgi import WSGIHandler
WSGIServer(WSGIHandler()).run()
