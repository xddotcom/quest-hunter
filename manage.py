#!/usr/local/bin/python2.7
# -*- coding: utf-8 -*-

import os
import sys

if __name__ == "__main__":
    sys.path += ['/home/content/98/10206198/py27venv/lib/python2.7/site-packages']
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "questhunter.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
